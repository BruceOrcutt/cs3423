// Defines
#define DEBUG 0
#define DEBUGR if(DEBUG) 
#define LINESIZE  255 
#define TABLESIZE 100
#define ARGNUMBER 10
#define EXITSTATUS 0

typedef struct proctable {
    unsigned int    pid;
    unsigned int    argcount;
    char            command[LINESIZE];
    char	    *arglines[LINESIZE];
    char            arguments[ARGNUMBER][LINESIZE];
//    char	    *arguments[LINESIZE];
} PROCTABLE;



// prototypes
int argStuffer();

