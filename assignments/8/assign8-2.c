#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include "assign8.h"
void argstuffer (char **arglines,PROCTABLE ptl,char args[ARGNUMBER][LINESIZE]) {
	int i;

	DEBUGR printf("ARGCOUNT is %d\n",ptl.argcount);
	for (i=1;i<=ptl.argcount;i++){
		DEBUGR printf("in argstuff, %d is i, %s arg to stuff\n",i,args[i]);
		arglines[i] = args[i];
	}
	
	arglines[0] = ptl.command;	
	arglines[i] = NULL;
	
}

int main(int argc, char *argv[]) {
    int         i,j,cmdFlag,cmdCount = 0;
    PROCTABLE   pt[TABLESIZE];
    char        line[LINESIZE];
    pid_t       child = 0;
    pid_t       parent = 0;
//    char	*arglines[LINESIZE];

    DEBUGR printf("Start\n");
    
    cmdFlag = 0;
    for (i = 1; i < argc; i++) {
        DEBUGR printf("%d: %s\n",i,argv[i]);
        sscanf(argv[i],"%s",line);
        DEBUGR printf("read arg as |%s|\n",line);

        DEBUGR printf("command flag is %d\n",cmdFlag);
	// split argments at commas
        if(line[0] == ',') {
            // a comma means reset and get ready for the next command
            //cmdCount++;
            cmdFlag = 0;
        } else {
            // not comma
            if(cmdFlag == 0){
                cmdCount++;
                pt[cmdCount].argcount = 0;
                pt[cmdCount].pid = 0;
                // cmdFlag 0 means we havent started collecting a command yet
                strcpy(pt[cmdCount].command,line);
                cmdFlag = 1;  // we are now processing a command
                strcpy(pt[cmdCount].arguments[0],line);
		//*pt[cmdCount].arguments = line;
                //pt[cmdCount].argcount++;
		        DEBUGR printf("Position 0 arg is %s\n",pt[cmdCount].arguments[0]);
		        pt[cmdCount].argcount = 0;
            } else {
                // not a command, must be arguments
          	    pt[cmdCount].argcount++;
	    	    DEBUGR printf("addl arg argcount should be 1 or more but is %d \n",pt[cmdCount].argcount);
                DEBUGR printf("%d argmument should be %s\n",pt[cmdCount].argcount,line);
                strcpy(pt[cmdCount].arguments[pt[cmdCount].argcount],line);
		//(*pt[cmdCount].arguments+pt[cmdCount].argcount) = line;
                DEBUGR printf("%d argument is now %s\n",pt[cmdCount].argcount,pt[cmdCount].arguments[pt[cmdCount].argcount]);
        //        pt[cmdCount].argcount++;
            } // cmdFlag
        } // if comma

    } // for args

    DEBUGR printf("We found %d commands\n",cmdCount);
    DEBUGR printf("arguments are \n");
    DEBUGR printf("==============\n");
    DEBUGR  for(i=1;i <= cmdCount;i++){
        printf("Command #%d: %s with %d arguments\n",cmdCount,pt[i].command,pt[i].argcount);
        for(j=0;j<=pt[i].argcount;j++){
            printf("%d: %s\n",j,pt[i].arguments[j]);
        } // argument for

    } // debug for

    DEBUGR printf("command count is %d\n",cmdCount);

    int FDM[2];  // file descriptors for pipe

    pipe(FDM);

    // now  let the forking commence
    for(i=1;i<=cmdCount;i++) {
	DEBUGR printf("-----------\n");
        pt[i].pid = fork();

	DEBUGR printf("%d is fork result\n",pt[i].pid);
        switch(pt[i].pid) {
            case -1:
                // error
                printf("Fork error %s\n",strerror(errno));
                break;
            case 0:
                // child
		    ;
//		    char	*arglines[LINESIZE];

                    child = getpid();
                    parent = getppid();

		    DEBUGR printf("my number is %d. My command is %s\n",i,pt[i].command);
		    DEBUGR printf("My argument 1 is %s\n",pt[i].arguments[1]);
		    
	            argstuffer(pt[i].arglines,pt[i],pt[i].arguments); 
                   
		    DEBUGR for(j=0;j<=pt[i].argcount;j++) {
		    	printf("child %ld argstuffed %d is %s\n",(long int)child,j,pt[i].arglines[j]);
		    } 
		    
//                    printf("PID: %ld, PPID: %ld, CMD: %s\n",(long int)child,(long int)parent,pt[i].command);

                    if(i==2) { 
                        // reader
                        dup2(FDM[0],0);
                        close(FDM[1]);
                    } else {
                        dup2(FDM[1],1);
                        close(FDM[0]);

                    }

                    //execvp(pt[i].command,(char * const* )pt[i].arguments);
		    //
		    DEBUGR printf("=====EXECING====\n");
		    execvp(pt[i].command,pt[i].arglines);

                break;
            default:
                // parent (skipped as children using exec, so later code not reached
                break;
        } // fork result switch
    } // forking for

    close(FDM[0]);
    close(FDM[1]);
    // parent only at this point due to exec in child
    for(i=1;i<=cmdCount;i++){
 //       if (wait(EXITSTATUS) == -1) {
 //           printf("Error waiting for child \n");
 //           continue;
 	
 //       }
 	wait(0);

    }
    return 0;
} // main

