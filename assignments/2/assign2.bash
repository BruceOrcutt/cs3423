#!/bin/bash

for f in $*
    do
        echo "processing $f"
        sed -i "s/<date>/`date +%mI%dI%Y`/g" $f
        sed -E -i "s/([0-9])I/\1\//g" $f
        sed -E -i -f exactlyone.sed $f 
    done
