//
// Created by csz860 on 11/13/19.
//

#ifndef INC_7_ASSIGN7_H
#define INC_7_ASSIGN7_H

#endif //INC_7_ASSIGN7_H

// define the course structure

#define NAMESIZE 64
#define SCHEDSIZE 4
#define COURSEFILE "courses.dat"
#define BUFFSIZE 100


typedef struct 
{
    char            courseName[NAMESIZE];
    char            courseSched[SCHEDSIZE];
    unsigned int    courseHours;
    unsigned int    courseSize;

} COURSE;
void    menu();
int     create();
int     update();
int     read();
int     del();
int     open();
int     cutLine();
int     lineread();




