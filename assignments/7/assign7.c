//
// Created by csz860 on 11/13/19.
//

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "assign7.h"

int cutLine(char *line) {
    // finds the newline, replaces with null to terminate the string
    int i;

    for (i = 0; i < strlen(line); i++) {
        if (line[i] == '\n') {
            line[i] = '\0';
            return 1;
        } // if
    } // for
    return 0;
} // cutLine

int lineread(char *line, FILE *file) {
// return values:
// 1  : success
// 0  : failure
    if (fgets(line, BUFFSIZE, file) == NULL)
        return 0;
    else {
        cutLine(line);
        return 1;
    } // else, fgets

} //lineread

int create() {
    int courseNumber;
    int courseLocation;
    char line[BUFFSIZE];
    COURSE course;      // hold the read course
    FILE *courseFile;

    // open files for read or write
    courseFile = fopen(COURSEFILE, "rb+");

    printf("Course Number: ");
    if (!lineread(line, stdin)) {
        printf("ERROR: Could not read course number line\n");
        fclose(courseFile);
        return -1;
    } else {
        if (sscanf(line, "%d", &courseNumber) != 1) {
            printf("ERROR: unable to read course number from input\n");
            fclose(courseFile);
            return -1;
        } // courseNumber
    } // else lineread

    printf("%d\n", courseNumber);
    // check for class existence
    courseLocation = (courseNumber - 1) * sizeof(course);

    // move the pointer to the position, after reset to the beginning
    fseek(courseFile, courseLocation, SEEK_SET);

    // read it
    fread(&course, sizeof(course), 1L, courseFile);

    if (course.courseName[0] != 0) {
        printf("ERROR: course already exists\n");
        fclose(courseFile);
        return -1;
    } // if fread course

    fflush(courseFile);

    // now to get the rest of the info for writing as we know the slot is free

//    courseLocation = findCourse();

    courseFile = fopen(COURSEFILE, "rb+");
    printf("Course name: ");
    if (!lineread(line, stdin)) {
        printf("ERROR: Could not read course name line\n");
        fclose(courseFile);
        return -1;
    } else {
        if (strcpy(course.courseName, line) == NULL) {
            printf("ERROR: unable to retrieve course name from input\n");
            fclose(courseFile);
            return -1;
        } // courseNumber
        printf("%s\n", course.courseName);
    } // else lineread

    printf("Course schedule: ");
    if (!lineread(line, stdin)) {
        printf("ERROR: Could not read course sched line\n");
        fclose(courseFile);
        return -1;
    } else {
        if (strcpy(course.courseSched, line) == NULL) {
            printf("ERROR: unable to retrieve course schedule from input\n");
            fclose(courseFile);
            return -1;
        } // courseSchedule
        printf("%s\n", course.courseSched);
    } // else lineread

    printf("Course hours: ");
    if (!lineread(line, stdin)) {
        printf("ERROR: Could not read course hours line\n");
        fclose(courseFile);
        return -1;
    } else {
        if (sscanf(line, "%d", &course.courseHours) == 0) {
            printf("ERROR: unable to retrieve hours from input\n");
            fclose(courseFile);
            return -1;
        } // courseHours
        printf("%d\n", course.courseHours);
    } // else lineread

    printf("Course enrollment: ");
    if (!lineread(line, stdin)) {
        printf("ERROR: Could not read course enrollment line\n");
        fclose(courseFile);
        return -1;
    } else {
        if (sscanf(line, "%d", &course.courseSize) == 0) {
            printf("ERROR: unable to retrieve course enrollment from input\n");
            fclose(courseFile);
            return -1;
        } // courseEnrollment
        printf("%d\n", course.courseSize);
    } // else lineread

    //        courseLocation = (courseNumber) * sizeof(course);
    fseek(courseFile, courseLocation, SEEK_SET);
    if (fwrite(&course, sizeof(course), 1L, courseFile) != 1) {
        printf("ERROR: couldn't write file after create.\n");
        fclose(courseFile);
        return -1;
    }


    fclose(courseFile);

} // create()

int read() {
    int courseNumber;
    int courseLocation;
    char line[BUFFSIZE];
    COURSE course;      // hold the read course
    FILE *courseFile;

    printf("Enter a CS course number: ");
    // read the line, then extract the number
    if (fgets(line, sizeof(line), stdin) != NULL) {
        if (!sscanf(line, "%d", &courseNumber)) {
            // Did we get an error in sscanf?
            printf("ERROR: Unable to detect a course number\n");
        } // if sscanf
        else {
            printf("%d\n", courseNumber);
            courseFile = fopen(COURSEFILE, "rb");

            // find the spot
            courseLocation = (courseNumber - 1) * sizeof(course);

            // move the pointer to the position, after reset to the beginning
            fseek(courseFile, courseLocation, SEEK_SET);

            // read it
            if (fread(&course, sizeof(course), 1L, courseFile) == 0) {
                printf("ERROR: Could not read values from %s", COURSEFILE);
                fclose(courseFile);
                return -2;
            } // if fread
            else {
                if (course.courseName[0] == 0) {
                    printf("ERROR: course not found\n");
                    fclose(courseFile);
                    return 0;
                } else {
                    printf("Course number: %d\n", courseNumber);
                    printf("Course name: %s\n", course.courseName);
                    printf("Scheduled days: %s\n", course.courseSched);
                    printf("Credit Hours: %u\n", course.courseHours);
                    printf("Enrolled Students: %u\n", course.courseSize);
                    fclose(courseFile);
                } // end else 0

            } // else fread
        } // else
    } // if fgets
    else {
        printf("ERROR: unable to read course number");
        return -1;
    } // else
} // read()

int del() {
    COURSE NULLcourse = {0};
    COURSE course;
    int courseNumber;
    int courseLocation;
    char line[BUFFSIZE];
    FILE *courseFile;

    printf("Enter a CS course number: ");
    // read the line, then extract the number
    //if (fgets(line, sizeof(line), stdin) != NULL) {
    if(!lineread(line,stdin)) {
        printf("ERROR: unable to read line in getting course number\n");
        return -1;
    } else {
        if (!sscanf(line, "%d", &courseNumber)) {
            // Did we get an error in sscanf?
            printf("ERROR: Unable to detect a course number\n");
        } // if sscanf
        else {
            printf("%d\n", courseNumber);
            courseFile = fopen(COURSEFILE, "rb+");

            // find the spot
            courseLocation = (courseNumber - 1) * sizeof(course);

            // move the pointer to the position, after reset to the beginning
            fseek(courseFile, courseLocation, SEEK_SET);

            // read it
            if (fread(&course, sizeof(course), 1L, courseFile) == 0) {
                printf("ERROR: Could not open %s", COURSEFILE);
                fclose(courseFile);
                return -2;
            } // if fread
            else {
                if (course.courseName[0] == 0) {
                    printf("ERROR: course not found\n");
                    fclose(courseFile);
                    return 0;
                } else {
                    fflush(courseFile);
                    fseek(courseFile, courseLocation, SEEK_SET);
                    course = NULLcourse;
                    if (fwrite(&course, sizeof(course), 1L, courseFile) != 1) {
                        printf("ERROR: couldn't write file after create.\n");
                        fclose(courseFile);
                        return -1;
                    } else {
                        printf("%d was successfully deleted\n",courseNumber);
                        fclose(courseFile);
                    }
                }// else coursename
            } // else fread
        } // else fgets
    } // else lineread
} // del()

int update() {
    int     courseNumber;
    int     courseLocation;
    char    line[BUFFSIZE];
    COURSE  course;      // hold the read course
    FILE    *courseFile;
    COURSE  tempCourse;

    printf("Enter a CS course number: ");

    // read the line, then extract the number
    if (fgets(line, sizeof(line), stdin) != NULL) {
        if (!sscanf(line, "%d", &courseNumber)) {
            // Did we get an error in sscanf?
            printf("ERROR: Unable to detect a course number\n");
        } // if sscanf
        else {
            printf("%d\n", courseNumber);
            courseFile = fopen(COURSEFILE, "rb+");

            // find the spot
            courseLocation = (courseNumber - 1) * sizeof(course);

            // move the pointer to the position, after reset to the beginning
            fseek(courseFile, courseLocation, SEEK_SET);

            // read it
            if (fread(&course, sizeof(course), 1L, courseFile) == 0) {
                printf("ERROR: Could not open %s", COURSEFILE);
                fclose(courseFile);
                return -2;
            } // if fread
            else {
                if (course.courseName[0] == 0) {
                    printf("ERROR: course not found\n");
                    fclose(courseFile);
                    return 0;
                } else {
                    printf("Course name: ");
                    if (!lineread(line, stdin)) {
                        printf("ERROR: Could not read course name line\n");
                        fclose(courseFile);
                        return -1;
                    } else {
//                        if (strcpy(tempCourse.courseName, line) != NULL) {
                          if (line[0] != 0) { 
                            // if not blank, we need to copy
                            //strcpy(course.courseName,tempCourse.courseName);
                            strcpy(course.courseName,line);
                        } // courseNumber
                        printf("%s\n", course.courseName);
                    } // else lineread

                    printf("Course schedule: ");
                    if (!lineread(line, stdin)) {
                        printf("ERROR: Could not read course sched line\n");
                        fclose(courseFile);
                        return -1;
                    } else {
                     //   if (strcpy(tempCourse.courseSched, line) != NULL) {
                        if (line[0] = 0) {
                            strcpy(course.courseSched,line);
                        } // courseSchedule
                        printf("%s\n", course.courseSched);
                    } // else lineread

                    printf("Course hours: ");
                    if (!lineread(line, stdin)) {
                        printf("ERROR: Could not read course hours line\n");
                        fclose(courseFile);
                        return -1;
                    } else {
                        if (sscanf(line, "%d", &tempCourse.courseHours) == 1) {
                            course.courseHours = tempCourse.courseHours;
                        } // courseHours
                        printf("%d\n", course.courseHours);
                    } // else lineread

                    printf("Course enrollment: ");
                    if (!lineread(line, stdin)) {
                        printf("ERROR: Could not read course enrollment line\n");
                        fclose(courseFile);
                        return -1;
                    } else {
                        if (sscanf(line, "%d", &tempCourse.courseSize) == 1) {
                            course.courseSize = tempCourse.courseSize;
                        } // courseEnrollment
                        printf("%d\n", course.courseSize);
                    } // else lineread

                    fseek(courseFile, courseLocation, SEEK_SET);

                    if (fwrite(&course, sizeof(course), 1L, courseFile) != 1) {
                        printf("ERROR: couldn't write file after create.\n");
                        fclose(courseFile);
                        return -1;
                    }
                    fclose(courseFile);
                } // end else 0

            } // else fread
        } // else
    } // if fgets
    else {
        printf("ERROR: unable to read course number");
        return -1;
    } // else

} // update()

void printMenu() {
    printf("Enter one of the following actions or press CTRL-D to exit.\n");
    printf("C - create a new course record\n");
    printf("R - read an existing course record\n");
    printf("U - update an existing course record\n");
    printf("D - delete an existing course record\n");
} // printMenu()

void menu() {
    char menuLine[BUFFSIZE];
    char menuChoice;

    menuChoice = 'X';

    while (menuChoice != 'Y') {

        // show the menu
        printMenu();

        // read the line, then extract the 1st character
        if (fgets(menuLine, sizeof(menuLine), stdin) != NULL) {

            if (!sscanf(menuLine, "%c", &menuChoice)) {
                // Did we get an error in sscanf?
                printf("ERROR: Unable to detect a character\n");

                break;
            } // if sscanf

            menuChoice = toupper(menuChoice);
            printf("%c\n", menuChoice);
            switch (menuChoice) {
                case 'C':
                    create();
                    break;
                case 'R':
                    read();
                    break;
                case 'U':
                    update();
                    break;
                case 'D':
                    del();
                    break;
                case EOF:
                    break;
                default:
                    printf("Invalid choice selected! %c\n", menuChoice);
            } // if switch
        } // if fgets
        else
            menuChoice = 'Y';

    } // while
} // menu


int main() {
    FILE *courses;

    // initialize file, create if does not exist
    courses = fopen(COURSEFILE, "ab");
    fclose(courses);

    menu();

    return 0;
}
