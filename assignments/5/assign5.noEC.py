#!/usr/bin/env python3

import os
import sys
import glob
import re

if len(sys.argv) < 5:
    print("ERROR: Not enough arguments")
    exit(1)

# put arguments in nicer named variables    
datadir     =   sys.argv[1]
template    =   sys.argv[2]
date        =   sys.argv[3]
outdir      =   sys.argv[4]

if  not os.path.isdir(outdir):
    os.mkdir(outdir)

# iterate over the datadir
for file in glob.glob(datadir+"/*"):
    # remove the path 
    file = file[file.find("/")+1::]

    # find the 1st digit, split on it's location
    numloc        = re.search("\d",file)
    dept_code     = file[0:numloc.start():]
    course_num    = file[numloc.start():numloc.start()+4:]

    # parse out the file
    with open(datadir + "/" + file,"r") as fp:
        lines           = fp.readlines()
        dept_code       = lines[0].split(None,1)[0].rstrip() 
        dept_name       = lines[0].split(None,1)[1].rstrip()
        course_name     = lines[1].rstrip()
        course_sched    = lines[2].split(None,2)[0].rstrip()
        course_start    = lines[2].split(None,2)[1].rstrip()
        course_end      = lines[2].split(None,2)[2].rstrip()
        credit_hours    = lines[3].rstrip()
        num_students    = lines[4].rstrip()
        
    if int(num_students) > 50:
        with open(template,"r") as tmpl:
            with open(outdir + "/" + dept_code + course_num + ".warn","w") as out:
                for tline in tmpl: 
                    tline = tline.replace("[[dept_code]]",dept_code)
                    tline = tline.replace("[[dept_name]]",dept_name)
                    tline = tline.replace("[[course_name]]",course_name)
                    tline = tline.replace("[[course_start]]",course_start)
                    tline = tline.replace("[[course_end]]",course_end)
                    tline = tline.replace("[[credit_hours]]",credit_hours)
                    tline = tline.replace("[[num_students]]",num_students)
                    tline = tline.replace("[[course_num]]",course_num) 
                    tline = tline.replace("[[date]]",date)

                    out.write(tline)
