import sys
import os
import glob
import pickle

# init variables to keep track
DEBUG   = False 
trash   = "/rm_trash/"              # trash name constant
rFlag   = False                     # recurse flag
homedir = os.path.expanduser("~")   # home directory
rmdir   = homedir + trash           # trash bin
cwd     = os.getcwd() + "/"         # get the cwd in case no absolute path
stem    = None                      # base file name
ext     = None                      # extension
fileWpath = None                    # filename + cwd
restDict = dict()                   # for restore, original path and name as key
dictFile = rmdir + ".ImPickleDict"  # filename for the dict pickle

if DEBUG:
    print("trash : "    +   trash)
    print("homedir : "  +   homedir)
    print("rmdir : "    +   rmdir)
    print("cwd : "      +   cwd)
    print("argv : "     +   str(sys.argv))
    print("argv len:"   +   str(len(sys.argv)))

# for restore, open the dict if it exists, otherwise blank dict
if os.path.exists(dictFile):
    with open(dictFile,"rb") as f:
        restDict= pickle.load(f)
# if not, already inited to empty Dict

# see if r flag was passed
if "-r" in sys.argv:
    rFlag = True
    sys.argv.remove("-r") 

if "DEBUGFLAG" in sys.argv:
    DEBUG = True
    sys.argv.remove("DEBUGFLAG")

if DEBUG:
    print(restDict)

if DEBUG:
    print("rFlag : "    +   str(rFlag))
    print("---------")

# were we given anything?
if len(sys.argv) < 2:
     sys.stderr.write("rm.py: no files to remove given\n")
     exit(1)
    

# create  the trash directory it if it does not exist
if not os.path.exists(rmdir):
    try:
        os.mkdir(rmdir)
    except OSError:
        sys.stderr.write("rm.py: Unable to create the ~/rm_trash file\n")


for file in sys.argv[1:]:
    if DEBUG:
        print("==========")
        print("File to delete: " + file)

    # exists?
    if not os.path.exists(file):
       sys.stderr.write("rm.py: cannot remove '" + file + "': No such file or directory\n")
       continue 

    # directory and not -r
    if os.path.isdir(file) and not rFlag:
       sys.stderr.write("rm.py: cannot remove '" + file + "': Is a directory\n")
       continue 

   # relative or absolute path? if relative make absolute.
    if file[0] != "/":
       fileWpath = cwd + file
    else:
       fileWpath = file

    if DEBUG:
       print("filename after path: " + fileWpath)

    # time for action

    # is this a file, not a directory?
    #if not os.path.isdir(file):
    if True:
        stem = None  # base file name
        ext = None  # extension

        (stem,ext) = os.path.splitext(file)

        stem = os.path.basename(stem)

        if DEBUG:
            print("filename:" + file + "splits into " + stem + " and " + ext)

        # Now see does it exist in the bucket in some form
        count = 0
        lastX = None
        x = "" 
        for x in glob.glob(rmdir + stem + "*" + ext):
            count += 1

        if not count == 0:
            stem = stem + "-" + str(count)

        delFile = rmdir + stem + ext

        if DEBUG:
            print("file is " + x  + " so new file will be " + delFile)
        
        try:
            os.rename(file,delFile)
            restDict[fileWpath] = delFile
            with open(dictFile,"wb") as f:
                pickle.dump(restDict,f)
        
        except OSError:
            sys.stderr.write("rm.py: OS Error attempting to relocate file\n")

        if DEBUG:
            print("dict contains: " + str(restDict))
