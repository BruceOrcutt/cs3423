import sys
import os
import glob
import pickle

# init variables to keep track
DEBUG   = False 
trash   = "/rm_trash/"              # trash name constant
homedir = os.path.expanduser("~")   # home directory
rmdir   = homedir + trash           # trash bin
cwd     = os.getcwd() + "/"         # get the cwd in case no absolute path
stem    = None                      # base file name
ext     = None                      # extension
fileWpath = None                    # filename + cwd
restDict = dict()                   # for restore, original path and name as key
dictFile = rmdir + ".ImPickleDict"  # filename for the dict pickle

if DEBUG:
    print("trash : "    +   trash)
    print("homedir : "  +   homedir)
    print("rmdir : "    +   rmdir)
    print("cwd : "      +   cwd)
    print("argv : "     +   str(sys.argv))
    print("argv len:"   +   str(len(sys.argv)))

# for restore, open the dict if it exists, otherwise blank dict
if os.path.exists(dictFile):
    try:
        with open(dictFile,"rb") as f:
            restDict= pickle.load(f)
    except OSError:
        sys.stderr.write("restore.py: No record of deleted files found\n")
# if not, already inited to empty Dict

if "DEBUGFLAG" in sys.argv:
    DEBUG = True
    sys.argv.remove("DEBUGFLAG")

if DEBUG:
    print(restDict)

# were we given anything?
if len(sys.argv) < 2:
     sys.stderr.write("restore.py: no files to restore given\n")
     exit(1)
    

# create  the trash directory it if it does not exist
if not os.path.exists(rmdir):
    sys.stderr.write("restore.py: Unable to access the ~/rm_trash directory\n")


for file in sys.argv[1:]:
    if DEBUG:
        print("==========")
        print("File to restore: " + file)

   # relative or absolute path? if relative make absolute.
    if file[0] != "/":
       fileWpath = cwd + file
    else:
       fileWpath = file

    if DEBUG:
       print("filename after path: " + fileWpath)

    if not fileWpath in restDict.keys():
        sys.stderr.write("restore.py: Cannot restore file " + fileWpath + ". File was never deleted.\n")
        continue

    resFile = restDict[fileWpath]

    if DEBUG:
        print("To restore " + fileWpath + " we restore " + resFile)

    if os.path.exists(fileWpath):
        sys.stderr.write("restore.py:  Error, file to restore already exists.  Not overwriting\n")
        continue

    try:
        os.rename(resFile,fileWpath)
        del restDict[fileWpath]
        with open(dictFile,"wb") as f:
            pickle.dump(restDict,f)
        
    except OSError:
        sys.stderr.write("restore.py: OS Error attempting to relocate file\n")

    if DEBUG:
        print("dict contains: " + str(restDict))
