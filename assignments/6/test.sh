python3 remove.py testdata/a testdata/1 testdata/test1
python3 remove.py testdata/a testdata/2 testdata/test2/a
echo "test2/a rewritten data" > testdata/test2/a
python3 remove.py testdata/b testdata/test2/a
python3 restore.py testdata/b testdata/test2/a testdata/a
python3 remove.py testdata/test3 -r 

ls -laR ~/rm_trash
cat ~/rm_trash/.rmdict
cat testdata/b testdata/test2/a testdata/a

