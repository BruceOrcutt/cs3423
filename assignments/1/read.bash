#!/bin/bash

#####
# read.bash
# reads a course
####

#variables
# deptcode: 2 or 3 character string
# deptname: String with whitespace
# csnum:    Course number
# csname:   Course name with whitespace
# cssch:    Course schedule, MWF or TH
# csstart:  Course start with slashes
# csend:    Course end with slashes
# cscredit: Course credit hours, unsigned int
# enroll:   Initial enrollment, unsigned int

# initial load
read deptcode deptname
echo "Course department: $deptcode $deptname"
echo "Course number:     $1"
read crsname
echo "Course name        $crsname"
read cssch csstart csend
echo "Scheduled days:    $cssch"
echo "Course start:      $csstart"
echo "Course end:        $csend"
read cscredit
echo "Credit hours:      $cscredit"
read enroll
echo "Enrolled Students: $enroll"
 


