#!/bin/bash

# variables
# input: initial command
# prompt: prompt for while loop read

# main menu

input=""
prompt='Enter one of the following actions or press CTRL-D to exit. 
       C - create a new course record                               
       R - read an existing course record                           
       U - update an existing course record                         
       D - delete an existing course record                         
       E - update enrolled student count of existing course         
       T - show total course count
       : ';                                 

while read -p "$prompt" input ;
    do
        input=${input^^};
        case $input in
            C)
                . ./create.bash
                ;;
            R)
                # initial load
                read -p "Enter a department code and course number:" deptcode csnum
                deptcode=${deptcode^^}
                filename="data/$deptcode$csnum"".crs"
             
                #file existance
                if [[ ! -f $filename ]]; then
                    echo "ERROR: course not found"
                else
                    . ./read.bash $csnum <$filename
                fi    
                
                # cleanup
                deptcode=""
                csnum=""
                filename=""
                ;;

            U)
                read -p "Department code:    " deptcode
                read -p "Department name:    " deptname
                read -p "Course number:      " crsnum
                read -p "Course name:        " crsname
                read -p "Course meeting days:" crssched
                read -p "Course start date:  " crsstart
                read -p "Course end date:    " crsend
                read -p "Course credit hours:" crscredit
                read -p "Course enrollment:  " enroll
                deptcode=${deptcode^^}
                filename="data/$deptcode$crsnum"".crs"

                if [[ ! -f $filename ]]; then
                    echo "ERROR: course not found"
                else
                    . ./update.bash < $filename
                fi

                deptcode=""
                deptname=""
                crsnum=""
                crsname=""
                crssched=""
                crsstart=""
                crsend=""
                crscredit=""
                enroll=""
                filename="":
                ;;
            D) 
                read -p "Enter a department code and course number:" deptcode csnum
                deptcode=${deptcode^^}
                filename="data/$deptcode$csnum"".crs"

                #file existance
                if [[ ! -f $filename ]]; then
                    echo "ERROR: course not found"
                else
                    . ./delete.bash < $filename
                fi 
        
                deptname=""
                crsname=""
                crssched=""
                crsstart=""
                crsend=""
                crscredit=""
                enroll=""
                filename=""
                deptcode=""
                csnum=""
                ;;

            E)
                read -p "Enter a department code and course number:" deptcode crsnum
                deptcode=${deptcode^^}
                filename="data/$deptcode$crsnum"".crs"
                read -p "Enter an enrollment change amount: " addenroll

                #file existance
                if [[ ! -f $filename ]]; then
                    echo "ERROR: course not found"
                else
                    . ./enroll.bash < $filename
                fi

                deptname=""
                crsname=""
                crssched=""
                crsstart=""
                crsend=""
                crscredit=""
                enroll=""
                filename=""
                deptcode=""
                csnum=""
                
                ;;

            T)
                . ./total.bash
                ;;

            *) 
                echo "ERROR: invalid option";
                input=""
                ;;
        esac
    done


