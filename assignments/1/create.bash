#!/bin/bash

#####
# create.bash
# Creates a course
####

#variables
# deptcode: 2 or 3 character string
# deptname: String with whitespace
# csnum:    Course number
# csname:   Course name with whitespace
# cssch:    Course schedule, MWF or TH
# csstart:  Course start with slashes
# csend:    Course end with slashes
# cscredit: Course credit hours, unsigned int
# enroll:   Initial enrollment, unsigned int

# initial load
read -p "Department Code:" deptcode
deptcode=${deptcode^^}
read -p "Department Name:" deptname
read -p "Course Number:"   csnum
read -p "Course Name:"     csname
read -p "Course Schedule:" cssch
cssch=${cssch^^}
read -p "Course Start:"    csstart
read -p "Course End:"      csend
read -p "Course Credit:"   cscredit
read -p "Initial enroll:"  enroll


filename="data/$deptcode$csnum"".crs"
#file existance
if [[ -f $filename ]]; then
    echo "ERROR: course already exists"
else 

# course file creation
    echo "$deptcode $deptname"    | cat > $filename  
    echo "$csname"                | cat >> $filename 
    echo "$cssch $csstart $csend" | cat >> $filename
    echo "$cscredit"              | cat >> $filename
    echo "$enroll"                | cat >> $filename
 

#log entry
    echo "[`date +%m/%d/%Y\ %H:%M`] CREATED: $deptcode $csnum $csname" | cat >> data/queries.log 
fi

