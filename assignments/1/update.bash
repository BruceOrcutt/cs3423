#!/bin/bash

read odeptcode odeptname
read ocrsname
read ocrssched ocrsstart ocrsend
read ocrscredit
read oenroll

if [[ $deptname  == "" ]]; then deptname=$odeptname;   fi
if [[ $crsname   == "" ]]; then crsname=$ocrsname;     fi
if [[ $crssched  == "" ]]; then crssched=$ocrssched;   fi    
if [[ $crsstart  == "" ]]; then crsstart=$ocrsstart;   fi
if [[ $crsend    == "" ]]; then crsend=$ocrsend;       fi
if [[ $crscredit == "" ]]; then crscredit=$ocrscredit; fi
if [[ $enroll    == "" ]]; then enroll=$oenroll;       fi

echo "$deptcode $deptname"         >  $filename
echo "$crsname"                    >> $filename
echo "$crssched $crsstart $crsend" >> $filename
echo "$crscredit"                  >> $filename
echo "$enroll"                     >> $filename

echo "[`date +%m/%d/%Y\ %H:%M`] UPDATED: $deptcode $crsnum $crsname" | cat >> data/queries.log

