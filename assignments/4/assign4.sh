#!/bin/bash

datadir=$1
template=$2
date=$3
outdir=$4
dept_code=""
course_num=""

for filename in `find  $datadir/* -printf "%f\n"`; do
    dept_code=`echo $filename | sed -E "s/([a-Z]+)([0-9]{4}).crs/\1/"`
    course_num=`echo $filename | sed -E "s/([a-Z]+)([0-9]{4}).crs/\2/"`


    for count in {1..5}; do    
        case $count in

            1)
                read line dept_name 
            ;;

            2)  
                read course_name 
            ;;
    
            3)
                read course_sched course_start course_end
            ;;
            
            4)
                read credit_hours
            ;;
    
            5)
                read num_students
            ;;
        esac


    done < $datadir/$filename
    
    if [ $num_students -gt 50 ]; then 
        cp $template $outdir/$dept_code$course_num.warn

        sed -i -e "s/\[\[dept_code\]\]/$dept_code/g"  $outdir/$dept_code$course_num.warn
        sed -i -e "s/\[\[dept_name\]\]/$dept_name/g"  $outdir/$dept_code$course_num.warn
        sed -i -e "s/\[\[course_name\]\]/$course_name/g"  $outdir/$dept_code$course_num.warn
        sed -i -e "s|\[\[course_start\]\]|$course_start|g"  $outdir/$dept_code$course_num.warn
        sed -i -e "s|\[\[course_end\]\]|$course_end|g"  $outdir/$dept_code$course_num.warn
        sed -i -e "s/\[\[course_hours\]\]/$course_hours/g"  $outdir/$dept_code$course_num.warn
        sed -i -e "s/\[\[num_students\]\]/$num_students/g"  $outdir/$dept_code$course_num.warn
        sed -i -e "s/\[\[course_num\]\]/$course_num/g"  $outdir/$dept_code$course_num.warn 
        sed -i -e "s|\[\[date\]\]|$date|g"  $outdir/$dept_code$course_num.warn
    fi
done



