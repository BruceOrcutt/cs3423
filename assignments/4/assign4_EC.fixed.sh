#!/bin/bash

# init 
datadir=$1
template=$2
date=$3
outdir=$4
dept_code=""
course_num=""
ldel="\[\[";
rdel="\]\]";

if [ ! -d $outdir ]; then
    mkdir $outdir
fi

#check for new delims
if [ $# -eq 6 ]; then
    ldel=$5
    rdel=$6
    ldel=`echo $ldel | sed -E "s=([\{\}])=\\\1=g"`
    rdel=`echo $rdel | sed -E "s=([\{\}])=\\\1=g"`
fi

for filename in `find  $datadir/* -printf "%f\n"`; do
    dept_code=`echo $filename | sed -E "s/([a-Z]+)([0-9]{4}).crs/\1/"`
    course_num=`echo $filename | sed -E "s/([a-Z]+)([0-9]{4}).crs/\2/"`


    for count in {1..5}; do    
        case $count in

            1)
                read line dept_name 
            ;;

            2)  
                read course_name 
            ;;
    
            3)
                read course_sched course_start course_end
            ;;
            
            4)
                read credit_hours
            ;;
    
            5)
                read num_students
            ;;
        esac


    done < $datadir/$filename
    
    if [ $num_students -gt 50 ]; then 
        cp $template $outdir/$dept_code$course_num.warn

        sed -i -e "s=${ldel}dept_code${rdel}=$dept_code=g"          $outdir/$dept_code$course_num.warn
        sed -i -e "s=${ldel}dept_name${rdel}=$dept_name=g"          $outdir/$dept_code$course_num.warn
        sed -i -e "s=${ldel}course_name${rdel}=$course_named=g"      $outdir/$dept_code$course_num.warn
        sed -i -e "s=${ldel}course_start${rdel}=$course_start=g"    $outdir/$dept_code$course_num.warn
        sed -i -e "s=${ldel}course_end${rdel}=$course_end=g"        $outdir/$dept_code$course_num.warn
        sed -i -e "s=${ldel}course_hours${rdel}=$course_hours=g"    $outdir/$dept_code$course_num.warn
        sed -i -e "s=${ldel}num_students${rdel}=$num_students=g"    $outdir/$dept_code$course_num.warn
        sed -i -e "s=${ldel}course_num${rdel}=$course_num=g"        $outdir/$dept_code$course_num.warn 
        sed -i -e "s=${ldel}date${rdel}=$date=g"                    $outdir/$dept_code$course_num.warn
    fi
done



