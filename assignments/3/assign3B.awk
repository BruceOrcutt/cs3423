BEGIN {
        prevabc = "";
        shortTime = 3000000000000;
        shortLine = "";
        longTime  = -1;
        longLine  = "";
}
$1 ~ /([a-z]{3}[0-9]{3})/ {
         abc = $1
         if (prevabc != abc) {
                print abc 
         }
         printf("\t%s",$8);
         for (i=9;i<=NF;i++) {printf( " %s",$i) }
         print "\n"
        prevabc = abc
        ("date -d" $5 " +%Y%m%d%H%M") | getline newTime 
        if (newTime < shortTime) { 
            shortTime=newTime;
            shortLine=$0;
        }
        if (newTime > longTime) {
            longTime=newTime;
            longLine=$0;
        }
}
END {
    print "\n";
    print "Earliest Start Time :\n"
    print shortLine;
    print "\n";
    print "Latest Start Time :\n"
    print longLine;
}

